#include <stdio.h>
#include <stdbool.h>

int main() {

    int smallest_num;
    int num;
    int n;
    char c;
    bool the_end = false;

    while (!the_end) {
        n = scanf("%d", &num);
        while (n < 1) {
            if (c != EOF || c == "\n" || c == " ") {
                c = getchar();
            }
            n = scanf("%d", &num);
        }
        if (c == EOF) {
            the_end = true;
        }

        else if (smallest_num > num)
            smallest_num = num;
    }


    printf("\nThe smallest number is %d\n", smallest_num);

    return 0;
}