#include <stdio.h>
#include <stdbool.h>


int main() {
    int first_int;
    int n;
    int scan_num;
    int num_count = 0;
    int position = 0;
    char c;
    bool the_end = false;

    scanf("%d", &scan_num);
    first_int = scan_num;

    while (!the_end) {
        n = scanf("%d", &scan_num);
        position +=1;
        while (n < 1) {
            if (c != EOF || c == "\n" || c == " ") {
                c = getchar();
                n = scanf("%d", &scan_num);
            }
        }
        c = getchar();
        if (c == EOF) {
            the_end = true;
        }

        if (scan_num == first_int) {
            num_count += 1;
            printf("Number %d was found at position %d \n", first_int, position);
        }

    }

    printf("\nNumber %d appeared %d times.", first_int, num_count);

    return 0;

}